-- Check with master looter active
-- break up the sorting over multiple frames

-- Data pulled from GetItemInfo, stored in tables with these indices
local IDX_SELLPRICE = 1
local IDX_ICON = 2
local IDX_BAGID = 3
local IDX_SLOT = 4
local IDX_ITEMCOUNT = 5
local IDX_ITEMCLASSID = 6
local IDX_ITEMSUBCLASSID = 7
local IDX_ITEMNAME = 8
local IDX_ITEMRARITY = 9
local IDX_ITEMID = 10

local LOOT_BUTTONS_VISIBLE = 5

-- Items earlier in the list are worth less
local RANK_TIER_SCORE = 32
local ITEM_RANKS = {
  {
    15, -- Miscellaneous
    {}  -- Currently bugged, mounts show as junk, so all same rank
  },
  {
    0, -- Consumable
    {
      4, -- Scroll 
      7, -- Bandage
      5, -- Food & Drink
      1, -- Potion
      0, -- Explosives and Devices
      2, -- Elixir
      6, -- Item Enhancement
      3, -- Flask
    }
  },
  {
    1, -- Container
    {} -- All same rank
  },
  {
    11, -- Quiver
    {} -- All same rank
  },
  {
    7, -- Tradeskill
    {} -- All same rank
  },
  {
    9, -- Recipe
    {} -- All same rank
  },
  {
    6, -- Projectile
    {} -- All same rank
  },
  {
    12, -- Quest
    {} -- All same rank
  },
  {
    8, -- Item Enhancement
    {} -- All same rank
  },
  {
    2, -- Weapon
    {} -- All same rank
  },
  {
    4, -- Armor
    {} -- All same rank
  },
  {
    13, -- Key
    {} -- All same rank
  }
}

SLASH_WTD1 = "/wtd"
function SlashCmdList.WTD(msg, editbox)
  if not LootFrame:IsVisible() then
    WhatToDelete:SetParent(UIParent)
    WhatToDelete:SetPoint("CENTER", UIParent, "CENTER")
    WhatToDelete:Show()
  end
end

function WhatToDeleteHideShow_OnClick(self, button, down)
  WhatToDelete:SetParent(LootFrame)
  WhatToDelete:SetPoint("TOPLEFT", LootFrame, "TOPRIGHT")
  if self.show then
    if button then -- skipped when programmatically called, assumming callers don't set this
      WhatToDelete:SetParent(LootFrame)
      WhatToDelete:SetPoint("TOPLEFT", LootFrame, "TOPRIGHT")
      WhatToDelete:Show()
    end
    self.show = false
    self:SetNormalTexture("Interface/Buttons/UI-SpellbookIcon-PrevPage-Up.PNG")
    self:SetPushedTexture("Interface/Buttons/UI-SpellbookIcon-PrevPage-Down.PNG")
    self:SetDisabledTexture("Interface/Buttons/UI-SpellbookIcon-PrevPage-Disabled.PNG")
    self:SetHighlightTexture("Interface/Buttons/UI-SpellbookIcon-PrevPage-Up.PNG")
  else
    if button then -- skipped when programmatically called, assumming callers don't set this
      WhatToDelete:Hide()
    end
    self.show = true
    self:SetNormalTexture("Interface/Buttons/UI-SpellbookIcon-NextPage-Up.PNG")
    self:SetPushedTexture("Interface/Buttons/UI-SpellbookIcon-NextPage-Down.PNG")
    self:SetDisabledTexture("Interface/Buttons/UI-SpellbookIcon-NextPage-Disabled.PNG")
    self:SetHighlightTexture("Interface/Buttons/UI-SpellbookIcon-NextPage-Up.PNG")
  end
end

function WhatToDeleteHideShow_OnShow(self)
  if self.show and WhatToDelete:IsVisible() then
    WhatToDeleteHideShow_OnClick(WhatToDeleteHideShow)
  end
end

function WhatToDelete_OnLoad(self)
  table.insert(UISpecialFrames, self:GetName()) -- Escape button functionality
  self:RegisterEvent("BAG_UPDATE")
  self:RegisterEvent("PLAYER_LOGIN")
  ButtonFrameTemplate_HidePortrait(WhatToDelete)
  ButtonFrameTemplate_HideAttic(WhatToDelete)
  ButtonFrameTemplate_HideButtonBar(WhatToDelete)
  WhatToDelete.list = {}
  if not WhatToDelete_Ignored then
    WhatToDelete_Ignored = {}
  end
  if not WhatToDelete_IgnoredChar then
    WhatToDelete_IgnoredChar = {}
  end
end

function WhatToDelete_OnHide(self)
  if WhatToDelete:GetParent() == LootFrame and LootFrame:IsVisible() then
    WhatToDeleteHideShow_OnClick(WhatToDeleteHideShow)
  end
end

function WhatToDelete_UpdateButtons()
  if WhatToDelete.page == 1 then
    WhatToDeletePrev:Hide()
    WhatToDeleteUpButton:Hide()
  else
    WhatToDeletePrev:Show()
    WhatToDeleteUpButton:Show()
  end

  local numItemsPerPage = LOOT_BUTTONS_VISIBLE
  if #WhatToDelete.list > LOOT_BUTTONS_VISIBLE then
    numItemsPerPage = numItemsPerPage - 1
  end

  if WhatToDelete.page == ceil(#WhatToDelete.list / numItemsPerPage) or #WhatToDelete.list <= LOOT_BUTTONS_VISIBLE then
    WhatToDeleteNext:Hide()
    WhatToDeleteDownButton:Hide()
  else
    WhatToDeleteNext:Show()
    WhatToDeleteDownButton:Show()
  end

  for i = 1, LOOT_BUTTONS_VISIBLE do
    local button = _G["WhatToDeleteLootButton" .. i]
    button.index = (WhatToDelete.page - 1) * numItemsPerPage + i
    if i <= numItemsPerPage and button.index < #WhatToDelete.list then
      local icon = _G["WhatToDeleteLootButton" .. i .. "IconTexture"]
      local text = _G["WhatToDeleteLootButton" .. i .. "Text"]
      local itemCount = _G["WhatToDeleteLootButton" .. i .. "Count"]
      icon:SetTexture(WhatToDelete.list[button.index][IDX_ICON])
      text:SetText(WhatToDelete.list[button.index][IDX_ITEMNAME])
      SetItemButtonNameFrameVertexColor(button, 0.5, 0.5, 0.5)
      SetItemButtonTextureVertexColor(button, 1.0, 1.0, 1.0)
      SetItemButtonNormalTextureVertexColor(button, 1.0, 1.0, 1.0)
      local color = ITEM_QUALITY_COLORS[WhatToDelete.list[button.index][IDX_ITEMRARITY]]
      text:SetVertexColor(color.r, color.g, color.b)
      if WhatToDelete.list[button.index][IDX_ITEMCOUNT] > 1 then
        itemCount:SetText(WhatToDelete.list[button.index][IDX_ITEMCOUNT])
        itemCount:Show()
      else
        itemCount:Hide()
      end
      button:Show()
    else
      button:Hide()
      button.index = nil
    end
  end
end

function WhatToDelete_PageDown()
  WhatToDelete.page = WhatToDelete.page + 1
  WhatToDelete_UpdateButtons()
end

function WhatToDelete_PageUp()
  WhatToDelete.page = WhatToDelete.page - 1
  WhatToDelete_UpdateButtons()
end

function WhatToDelete_OnShow(self)
  WhatToDelete.page = 1
  WhatToDelete_UpdateButtons()
end

function WhatToDelete_OnEnter(self)
  if self.index then
    GameTooltip:SetOwner(self)
    GameTooltip:SetBagItem(
      WhatToDelete.list[self.index][IDX_BAGID],
      WhatToDelete.list[self.index][IDX_SLOT]
    )
    CursorUpdate(self);
  end
end

function WhatToDelete_OnClick(self, button)
  if self.index then
    if button == "LeftButton" then
      PickupContainerItem(
        WhatToDelete.list[self.index][IDX_BAGID],
        WhatToDelete.list[self.index][IDX_SLOT]
      )
    elseif button == "MiddleButton" then
      WhatToDelete_IgnoredChar[WhatToDelete.list[self.index][IDX_ITEMID]] = WhatToDelete.list[self.index][IDX_ITEMNAME]
      WhatToDelete_BagUpdate()
      if WhatToDelete:IsVisible() then
        WhatToDelete_UpdateButtons()
      end
    end
  end
end

function WhatToDelete_OnDoubleClick(self, button)
  if self.index then
    if button == "RightButton" then
      PickupContainerItem(
        WhatToDelete.list[self.index][IDX_BAGID],
        WhatToDelete.list[self.index][IDX_SLOT]
      )
      DeleteCursorItem();
    elseif button == "MiddleButton" then
      -- WhatToDelete_Ignored[WhatToDelete.list[self.index][IDX_ITEMID]] = WhatToDelete.list[self.index][IDX_ITEMNAME]
      -- WhatToDelete_BagUpdate()
      -- if WhatToDelete:IsVisible() then
      --   WhatToDelete_UpdateButtons()
      -- end
    end
  end
end

function WhatToDelete_OnEvent(self, event, ...)
  if event == "BAG_UPDATE" or event == "PLAYER_LOGIN" then
    WhatToDelete_BagUpdate()
    if WhatToDelete:IsVisible() then
      WhatToDelete_UpdateButtons()
    end
  end
end

function WhatToDelete_BagUpdate()
  WhatToDelete.list = {}

  for bagID = BACKPACK_CONTAINER, BACKPACK_CONTAINER + 4 do
    for slot = 0, GetContainerNumSlots(bagID) do
      -- Record the relevant info for each slot to determine its value, nil if empty
      local itemID = GetContainerItemID(bagID, slot)
      if itemID ~= nil and WhatToDelete_Ignored[itemID] == nil and WhatToDelete_IgnoredChar[itemID] == nil then -- is slot empty or blocklisted
        local IDX_SLOTINFO_ITEMCOUNT = 2
        local slotInfo = {GetContainerItemInfo(bagID, slot)}

        local IDX_ITEMINFO_ITEMNAME = 1
        local IDX_ITEMINFO_ITEMRARITY = 3
        local IDX_ITEMINFO_ITEMCLASSID = 12
        local IDX_ITEMINFO_ITEMSUBCLASSID = 13
        local IDX_ITEMINFO_ICON = 10
        local IDX_ITEMINFO_SELLPRICE = 11
        local itemInfo = {GetItemInfo(itemID)}

        if not itemInfo[IDX_ITEMINFO_SELLPRICE] then
          itemInfo[IDX_ITEMINFO_SELLPRICE] = 0
        end

        table.insert(
          WhatToDelete.list,
          {
            itemInfo[IDX_ITEMINFO_SELLPRICE],
            itemInfo[IDX_ITEMINFO_ICON],
            bagID,
            slot,
            slotInfo[IDX_SLOTINFO_ITEMCOUNT],
            itemInfo[IDX_ITEMINFO_ITEMCLASSID],
            itemInfo[IDX_ITEMINFO_ITEMSUBCLASSID],
            itemInfo[IDX_ITEMINFO_ITEMNAME],
            itemInfo[IDX_ITEMINFO_ITEMRARITY],
            itemID
          }
        )
      end
    end
  end
  table.sort(WhatToDelete.list, WhatToDelete_ComparePrices)
end

local once = false
function WhatToDelete_ComparePrices(a, b)
  if not a or not b then
    return false
  end

  -- Get the base item rank
  local rankA = 0
  local rankB = 0
  if once then
    print(a[IDX_ITEMCLASSID], b[IDX_ITEMCLASSID])
  end
  
  for i, v in ipairs(ITEM_RANKS) do
    if v[1] == a[IDX_ITEMCLASSID] then
      rankA = RANK_TIER_SCORE * i -- Items earlier in the list are worth less
      for ii, vv in ipairs(v[2]) do
        if vv == a[IDX_ITEMSUBCLASSID] then
          rankA = rankA + ii -- Items earlier in the list are worth less
          break
        end
      end
      if once then
        print("a found", v[1])
      end
    end
    if v[1] == b[IDX_ITEMCLASSID] then
      rankB = RANK_TIER_SCORE * i -- Items earlier in the list are worth less
      for ii, vv in ipairs(v[2]) do
        if vv == b[IDX_ITEMSUBCLASSID] then
          rankB = rankB + ii -- Items earlier in the list are worth less
          break
        end
      end
      if once then
        print("b found", v[1])
      end
    end
  end
  if once then
    print(rankA, rankB)
    once = false
  end

  -- Account for rarity level
  if a[IDX_ITEMRARITY] then
    rankA = rankA + RANK_TIER_SCORE * a[IDX_ITEMRARITY]
  end
  if b[IDX_ITEMRARITY] then
    rankB = rankB + RANK_TIER_SCORE * b[IDX_ITEMRARITY]
  end

  rankA = rankA + a[IDX_SELLPRICE] * a[IDX_ITEMCOUNT]
  rankB = rankB + b[IDX_SELLPRICE] * b[IDX_ITEMCOUNT]

  return rankA < rankB

  -- -- Order based on rank
  -- if rankA ~= rankB then
  --   return rankA < rankB
  -- end

  -- -- Order based on price
  -- return a[IDX_SELLPRICE] * a[IDX_ITEMCOUNT] < b[IDX_SELLPRICE] * b[IDX_ITEMCOUNT]
end
