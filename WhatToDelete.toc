## Interface: 11308
## Title: What To Delete
## Author: Softea-Lethon
## Version: 1.0.0
## Notes: Shows the best item to delete for bag space.
## SavedVariables: WhatToDelete_Ignored
## SavedVariablesPerCharacter: WhatToDelete_IgnoredChar

WhatToDelete.xml
